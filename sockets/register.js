const models = require("../models");
const emailCheck = require("./utils/emailCheck");

module.exports = socket => {
    return async (data) => {
        if (await models.User.findByPk(data.email))
            socket.emit("register", {error: {message: "email_used"}});
        else if ((!data.email.endsWith("@univ-lyon1.fr")) && (!data.email.endsWith("@etu.univ-lyon1.fr")))
            socket.emit("register", {error: {message: "invalid_email"}});
        else {
            try {
                let user = await models.User.create({
                    email: data.email,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    passwordHash: data.password
                });
                await emailCheck(socket, user, null);
            } catch (Exception) {
                socket.emit("register", {error: {message: "invalid_email"}})
            }
        }
    }
};
