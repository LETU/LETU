const models = require("../models");

module.exports = socket => {
    return async (data) => {
        let user = await models.User.findByPk(data.email, {include: {model: models.Group, include: models.Semester}});
        if (!user)
            socket.emit("login", {error: {message: "not_found"}});
        else if (!user.checkPassword(data.password))
            socket.emit("login", {error: {message: "invalid_password"}});
        else if (!user.emailVerified)
            socket.emit("login", {error: {message: "email_not_verified"}});
        else {
            socket.request.session.user = user;
            socket.request.session.save();
            socket.emit("login", user)
        }
    }
};
