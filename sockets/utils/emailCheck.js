let crypto = require("crypto");
let models = require("../../models");
let Message = require("emailjs").Message;
const config = require("../../config/config.json");


module.exports = async (socket, user, callBack) => {
    let token = crypto.randomBytes(16).toString("hex");

    while (await models.User.findOne({where: {emailToken: token}}))
        token = crypto.randomBytes(16).toString("hex");
    user.emailToken = token;
    await user.save();

    socket.server.mailClient.send( new Message({
        text: `${config.email.mailPath}/email/check?token=${token}`,
        from: config.email.from,
        to: user.email,
        subject: "Email check"
    }), (err, message) => {
        if (err)
            socket.emit("register", null);
        else
            if (callBack)
                callBack();
    });
};
