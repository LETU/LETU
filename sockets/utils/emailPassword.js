let crypto = require("crypto");
let models = require("../../models");
let Message = require("emailjs").Message;
const config = require("../../config/config.json");


module.exports = async (socket, user, callBack) => {
    let token = crypto.randomBytes(16).toString("hex");
    while (await models.User.findOne({where: {passwordToken: token}}))
        token = crypto.randomBytes(16).toString("hex");

    socket.server.mailClient.send( new Message({
        text: `${config.email.mailPath}/email/forget?token=${token}`,
        from: config.email.from,
        to: user.email,
        subject: "forgot password"
    }), async (err, message) => {
        if (err)
            socket.emit("forgotPassword", {error: {message: "fail_send_mail"}});
        else {
            user.passwordToken = token;
            user.passwordTokenDate = new Date();
            await user.save();
            socket.emit("forgotPassword", true);
        }
    });
};
