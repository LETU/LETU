const models = require("../../models");

module.exports = socket => {
    return async (data) => {
        const options = {where: {},
            include: [models.UE, {model: models.User},
                {
                    model: models.Grade,
                    include: [{attributes: ["email", "firstName", "lastName"], model: models.User, as: "TeacherGrade"},
                        {attributes: ["email", "firstName", "lastName"], model: models.User, as: "StudentGrade"}]
                }]
        };

        if (data && data.id)
            options.where.id = data.id;

        if (socket.request.session.user.permissions === 2) {
                options.include[0].through = {where: {UserEmail: socket.request.session.user.email}};
                options.include[0].required = true;
        }
        socket.emit("evaluationGet", await models.Evaluation.findAll(options));
    }
};
