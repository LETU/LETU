const models = require("../../models");

module.exports = socket => {
    return async (data) => {
        let options = {where: {}};
        if (data.email)
            options.where.email = data.email;
        if (data.firstName && data.lastName) {
            options.where.firstName = data.firstName;
            options.where.lastName = data.lastName;
        }
        if (data.permissions)
            options.where.permissions = data.permissions;

        socket.emit("userGet", await models.User.findAll(options));
    }
};
