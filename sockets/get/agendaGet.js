const models = require("../../models");


function addWhere(options, index, attr, value) {
    if (!options.include[index].where)
        options.include[index].where = {};
    if (!options.include[index].where[attr])
        options.include[index].where[attr] = value;
    else {
        if (options.include[index].where[attr] !== [models.Sequelize.Op.in])
            options.include[index].where[attr] = {[models.Sequelize.Op.in]: [options.include[index].where[attr]]};
        options.include[index].where[attr][models.Sequelize.Op.in].push(value);
    }
    options.include[index].require = true;
}

module.exports = socket => {
    return async (data) => {
        let options = {
            where: {},
            include: [
                {model: models.User, attributes: ["email", "firstName", "lastName"]},
                {model: models.Group, include: models.Semester},
                {model: models.Semester, include: {model: models.Group, include: models.Semester}}
                ]
        };

        if (data && data.startDate && data.endDate)
            options.where = {startDate: {[models.Sequelize.Op.between]: [data.startDate, data.endDate]}, endDate: {[models.Sequelize.Op.between]: [data.startDate, data.endDate]}};

        if (data && data.semesters) {
            for (let semester of data.semesters) {
                let s = await models.Semester.findByPk(semester);
                if (!s) {
                    socket.emit("agendaGet", {error: {message: "semester_not_found"}});
                    return
                } else
                    addWhere(options, 2, "id", s.id);
            }
        }
        if (data && data.groups) {
            for (let group of data.groups) {
                let g = await models.Group.findByPk(group);
                if (!g) {
                    socket.emit("agendaGet", {error: {message: "group_not_found"}});
                    return
                } else
                    addWhere(options, 1, "id", g.id);
            }
        }
        if (socket.request.session.user.permissions === 2)
            if (data && !data.teachers)
                data.teachers = [socket.request.session.user.email];
        if (data && data.teachers) {
            for (let teacher of data.teachers) {
                let t = await models.User.findByPk(teacher);
                if (!t) {
                    socket.emit("agendaGet", {error: {message: "teacher_not_found"}});
                    return
                } else
                    addWhere(options, 0, "email", t.email);
            }
        }
        socket.emit("agendaGet", await models.Event.findAll(options));
    }
};
