const models = require("../../models");

module.exports = socket => {
    return async (data) => {
        let options = {include: [{model: models.Semester}]};
        if (data && (data.id || data.number || data.semester))
            options.where = {};
        if (data && data.id)
            options.where.id = data.id;
        if (data && data.number)
            options.where.number = data.number;
        if (data && data.semester) {
            let s = await models.Semester.findByPk(data.semester);
            if (!s) {
                socket.emit("groupGet", {error: {message: "semester_not_found"}});
                return
            }
            options.include[0].where = {id: s.id};
            options.include[0].require = true;
        }
        if (data && data.users)
            options.include.push({model: models.User, where: {email: {[models.Sequelize.Op.in]: data.users}}, require: true});

        socket.emit("groupGet", await models.Group.findAll(options));
    }
};
