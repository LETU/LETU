const models = require("../../models");

module.exports = socket => {
    return async (data) => {
        let options = {include: [{model: models.Group}]};
        if (data && (data.id || data.name || data.year))
            options.where = {};
        if (data && data.id)
            options.where.id = data.id;
        if (data && data.name)
            options.where.name = data.name;
        if (data && data.year)
            options.where.year = data.year;
        if (data && data.groups)
            options.include[0].where = {id: {[models.Sequelize.Op.or]: data.groups}};

        socket.emit("semesterGet", await models.Semester.findAll(options));
    }
};
