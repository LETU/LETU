const models = require("../../models");

module.exports = socket => {
    return async (data) => {
        let options = {
            attributes: ["email", "firstName", "lastName"],
            where: {permissions: 1},
            include: [{model: models.Group, include: models.Semester}]
        };
        if (data && data.email)
            options.where.email = data.email;
        if (data && data.firstName && data.lastName) {
            options.where.firstName = data.firstName;
            options.where.lastName = data.lastName;
        }
        if (data && data.group) {
            options.include[0].where = {id: data.group};
            options.include[0].required = true;
        }

        socket.emit("studentGet", await models.User.findAll(options));
    }
};
