const models = require("../../models");

module.exports = socket => {
    return async (data) => {
        const options = {where: {}, include: [{model: models.Evaluation, include: {model: models.UE}},
                {attributes: ["email", "firstName", "lastName"], model: models.User, as: "TeacherGrade"},
                {attributes: ["email", "firstName", "lastName"], model: models.User, as: "StudentGrade", include: {model: models.Group, include: models.Semester}}]};

        if (data && data.id)
            options.where.id = data.id;

        switch (socket.request.session.user.permissions) {
            case 1:
                options.where.StudentGradeEmail = socket.request.session.user.email;
                break;
            case 2:
                options.where.TeacherGradeEmail = socket.request.session.user.email;
                break;
        }
        socket.emit("gradeGet", await models.Grade.findAll(options));
    }
};
