module.exports = socket => {
    console.log("New connection !");
    if (!socket.request.session.user) {
        socket.on("login", require("./login")(socket));
        socket.on("register", require("./register")(socket));
        socket.on("checkResend", require("./email/checkResend")(socket));
        socket.on("forgotPassword", require("./email/forgotPassword")(socket));
        socket.on("setPassword", require("./email/setPassword")(socket));
    } else {
        socket.on("profileEdit", require("./profile/edit")(socket));
        socket.on("logout", require("./logout")(socket));
        socket.on("agendaGet", require("./get/agendaGet")(socket));
        socket.on("gradeGet", require("./get/gradeGet")(socket));
        if (socket.request.session.user.permissions > 1) {
            socket.on("evaluationGet", require("./get/evaluationGet")(socket));
            socket.on("gradeSet", require("./set/gradeSet")(socket));
            socket.on("studentGet", require("./get/studentGet")(socket));
        }
        if (socket.request.session.user.permissions > 2) {
            socket.on("groupGet", require("./get/groupGet")(socket));
            socket.on("semesterGet", require("./get/semesterGet")(socket));
            socket.on("userSet", require("./set/userSet")(socket));
        }
    }
    socket.emit("connected");
};
