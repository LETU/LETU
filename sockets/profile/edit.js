const models = require("../../models");

module.exports = socket => {
    return async (data) => {
        let user = await models.User.findByPk(socket.request.session.user.email);
        if (!user)
            socket.emit("profileEdit", {error: {message: "not_found"}});
        else if (!user.checkPassword(data.oldPassword))
            socket.emit("profileEdit", {error: {message: "invalid_password"}});
        else {
            if (data.firstName !== user.firstName)
                user.firstName = data.firstName;
            if (data.lastName !== user.lastName)
                user.lastName = data.lastName;
            user.newPassword = data.newPassword;
            if (data.password && !user.checkPassword(data.password))
                user.passwordHash = data.password;
            socket.request.session.user = user;
            socket.request.session.save();
            socket.emit("profileEdit", user)
        }
    }
};
