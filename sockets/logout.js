module.exports = socket => {
    return async (data) => {
        if(socket.request.session.user){
            socket.request.session.destroy();
            socket.emit("logout", true);
        }else{
            socket.emit("logout", {error: { message: "not_logged_in"}});
        }
    }
};