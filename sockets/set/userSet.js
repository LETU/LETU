const models = require("../../models");

module.exports = socket => {
    return async (data) => {
        if (!data || !data.user) {
            socket.emit("userSet", {error: {message: "missing_arguments"}});
            return;
        }
        let u = await models.User.findByPk(data.user);
        if (!u) {
            socket.emit("userSet", {error: {message: "user_not_found"}});
            return;
        }
        for (let i in data)
            switch (i) {
                case "firstName":
                    u.firstName = data.firstName;
                    break;
                case "lastName":
                    u.lastName = data.lastName;
                    break;
                case "password":
                    u.passwordHash = data.password;
                    break;
                case "permissions":
                    u.permissions = data.permissions;
                    break;
                case "groups":
                    let groups = [];
                    for (let g of data.groups) {
                        let gp = await models.Group.findByPk(g);
                        if (!gp) {
                            socket.emit("userGet", {error: {message: "group_not_found"}});
                            return
                        }
                        groups.push(gp);
                    }
                    u.setGroups(groups);
                    break;
            }
        await u.save();
    }
};
