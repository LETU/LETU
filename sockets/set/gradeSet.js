const models = require("../../models");

module.exports = socket => {
    return async (data) => {
        if (!data || !data.evaluation || !data.grades) {
            socket.emit("gradeSet", {error: {message: "missing_arguments"}});
            return;
        }
        for (let student in data.grades) {
            let grade = await models.Grade.findOne({where: {StudentGradeEmail: student, EvaluationId: data.evaluation}});
            if (!grade) {
                await models.Grade.create({TeacherGradeEmail: socket.request.session.user.email, StudentGradeEmail: student, EvaluationId: data.evaluation, score: data.grades[student], limit: 20})
            } else {
                grade.score = data.grades[student];
                await grade.save();
            }
        }
    }
};
