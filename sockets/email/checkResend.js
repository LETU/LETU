const models = require("../../models");
const emailCheck = require("../utils/emailCheck");

module.exports = socket => {
    return async (data) => {
        let user = await models.User.findByPk(data.email);
        if (!user)
            socket.emit("checkResend", {error: {message: "not_found"}});
        else if (user.emailVerified)
            socket.emit("checkResend", {error: {message: "verified_email"}});
        else
            await emailCheck(socket, user, null);
    }
};
