const models = require("../../models");


module.exports = socket => {
    return async (data) => {
        let user = await models.User.findOne({where: {passwordToken: data.token}});
        if (!user)
            socket.emit("setPassword", {error: {message: "invalid_token"}});
        else if (user.passwordTokenDate && ((new Date().getTime() - user.passwordTokenDate.getTime()) / 1000 > 3600))
            socket.emit("setPassword", {error: {message: "expired_token"}});
        else {
            user.passwordToken = null;
            user.passwordTokenDate = null;
            user.passwordHash = data.password;
            await user.save();
            socket.emit("setPassword", true);
        }
    }
};
