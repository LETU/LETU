const models = require("../../models");
const emailPassword = require("../utils/emailPassword");


module.exports = socket => {
    return async (data) => {
        let user = await models.User.findByPk(data.email);
        if (!user)
            socket.emit("forgotPassword", {error: {message: "not_found"}});
        else
            await emailPassword(socket, user, null);
    }
};
