let request = require("supertest");
let wipeDatabase = require("./utils/wipeDatabase");


async function setup() {
    let app = require("../app");
    let models = require("../models");
    await models.sequelize.sync();
    await wipeDatabase(models);
    return [app, models];
}

async function clean() {
    await wipeDatabase(models);
    await models.sequelize.close();
    for (let e of ["../app", "../models"])
        delete require.cache[require.resolve(e)];
}


before(async () => {
    [app, models] = await setup();
});

it("Main page content", async () => {
    await request(app)
        .get("/")
        .expect(302);
});
it("Login page content", async () => {
    await request(app)
        .get("/login")
        .expect(200);
});

after(() => {
    return clean;
});
