const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const session = require("express-session");
const error = require("./routes/utils/error");
const config = process.env.NODE_ENV === "test" ? {} : require("./config/config.json");
const models = require("./models");

const indexRouter = require("./routes/index");
const loginRouter = require("./routes/login");
const edtRouter = require("./routes/edt");
const marksRouter = require("./routes/marks");
const registerRouter = require("./routes/register");
const viescolRouter = require("./routes/viescol");
const profilRouter = require("./routes/profil");

let app = express();
const sessionMiddleware = session({
  secret: process.env.NODE_ENV === "test" ? "Keyboard Cat" : config.secret,
  resave: false,
  saveUninitialized: true
});

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.set("sessionMiddleware", sessionMiddleware);
app.set("config", config);

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(sessionMiddleware);
app.use(async (req, res, next) => {
  if (req.session.user) {
    let user = await models.User.findByPk(req.session.user.email, {include: {model: models.Group, include: models.Semester}});
    if (user.updatedAt !== req.session.user.updatedAt) {
      req.session.user = user;
      req.session.save();
    }
  }
  res.locals.session = req.session;
  next();
});

app.use("/", indexRouter);
app.use("/login", loginRouter);
app.use("/email", require("./routes/email"));
app.use("/edt", edtRouter);
app.use("/marks", marksRouter);
app.use('/register', registerRouter);
app.use('/viescol', viescolRouter);
app.use('/profil', profilRouter);

// catch 404 and forward to error handler
app.use((req, res) => {
  return error(req, res, "Page not found", 404);
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
