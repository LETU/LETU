"use strict";

const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class Semester extends Model {
        static associate(models) {
            Semester.hasMany(models.Group, {foreignKey: {allowNull: false}});
            Semester.belongsToMany(models.Event, {through: "EventSemester"});
        }
    }
    Semester.init({
        year: {
            type: DataTypes.INTEGER,
            allowNull: false,
            unique: "semester"
        },
        name : {
            type: DataTypes.STRING,
            allowNull: false,
            unique: "semester"
        }
    }, {
        sequelize,
        modelName: "Semester",
    });
    return Semester;
};
