"use strict";

const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class Group extends Model {
        static associate(models) {
            Group.belongsTo(models.Semester, {foreignKey: {allowNull: false}});
            Group.belongsToMany(models.User, {through: "UserGroup"});
            Group.belongsToMany(models.Event, {through: "EventGroup"});
        }
    }
    return Group.init({
        number: {
            type: DataTypes.STRING,
            allowNull: false
        },
        displayName: {
            type: DataTypes.VIRTUAL,
            get() {
                if (this.number.startsWith("G"))
                    return this.number + this.Semester.name;
                else
                    return this.Semester.name + " " + this.number
            }
        }
    }, {
        sequelize,
        modelName: "Group"
    });
};
