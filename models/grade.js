"use strict";

const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class Grade extends Model {
        static associate(models) {
            Grade.belongsTo(models.Evaluation);
            Grade.belongsTo(models.User, {as: "TeacherGrade"});
            Grade.belongsTo(models.User, {as: "StudentGrade"});
        }
    }
    Grade.init({
        score: {
            type: DataTypes.FLOAT,
            allowNull: false
        },
        limit: {
            type: DataTypes.FLOAT,
            allowNull: false
        },
        comment: {
            type: DataTypes.TEXT
        }
    }, {
        sequelize,
        modelName: "Grade",
    });
    return Grade;
};
