"use strict";

const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class Event extends Model {
        static associate(models) {
            Event.belongsToMany(models.Semester, {through: "EventSemester"});
            Event.belongsToMany(models.Group, {through: "EventGroup"});
            Event.belongsToMany(models.User, {through: "EventTeacher"});
            Event.belongsTo(models.UE);
        }
    }
    Event.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        locations: {
            type: DataTypes.STRING,
            allowNull: false
        },
        startDate: {
            type: DataTypes.DATE,
            allowNull: false
        },
        endDate: {
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        sequelize,
        modelName: "Event",
    });
    return Event;
};
