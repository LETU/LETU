"use strict";

const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class UE extends Model {
        static associate(models) {
            UE.hasMany(models.Event);
            UE.hasMany(models.Evaluation);
            UE.belongsToMany(models.User, {through: "UEUser"});
        }
    }
    UE.init({
        name : {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        }
    }, {
        sequelize,
        modelName: "UE",
    });
    return UE;
};
