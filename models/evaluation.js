"use strict";

const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class Evaluation extends Model {
        static associate(models) {
            Evaluation.belongsToMany(models.User, {through: "EvaluationTeacher"});
            Evaluation.hasMany(models.Grade);
            Evaluation.belongsTo(models.UE);
        }
    }
    Evaluation.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        date: {
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        sequelize,
        modelName: "Evaluation",
    });
    return Evaluation;
};
