"use strict";

const crypto = require("crypto");
const privateKey = process.env.NODE_ENV === "test" ? require("../config/config_example.json") : require("../config/config.json").passwordPrivateKey;

const {
    Model
} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    function hash(password, email) {
        const cipher = crypto.createCipheriv(
            "aes-256-cbc",
            privateKey,
            crypto.createHash("md5").update(email).digest("base64").slice(0, 16)
        );
        return cipher.update(password, "utf8", "base64") + cipher.final("base64");
    }

    class User extends Model {
        static associate(models) {
            User.belongsToMany(models.Group, {through: "UserGroup"});
            User.belongsToMany(models.Event, {through: "EventTeacher"});
            User.belongsToMany(models.Evaluation, {through: "EvaluationTeacher"});
            User.belongsToMany(models.UE, {through: "UEUser"});
        }

        checkPassword(password) {
            return hash(password, this.email) === this.passwordHash
        }
    }
    User.init({
        email: {
            type: DataTypes.STRING,
            validate: {
                isEmail: true
            },
            primaryKey: true
        },
        emailVerified : {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        emailToken: {
            type: DataTypes.STRING,
            unique: true
        },
        firstName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        lastName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        passwordHash: {
            type: DataTypes.STRING,
            allowNull: false,
            set(value) {
                if (value)
                    this.setDataValue("passwordHash", hash(value, this.email));
            }
        },
        passwordToken: {
            type: DataTypes.STRING,
            unique: true
        },
        passwordTokenDate: {
            type: DataTypes.DATE
        },
        permissions: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
            allowNull: false
        }
    }, {
        sequelize,
        modelName: "User",
    });
    return User;
};
