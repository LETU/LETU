const express = require("express");
const router = express.Router();
const models = require("../models");
const error = require("./utils/error");
const sessionCheck = require("./utils/sessionCheck");


router.get("/check", async (req, res) => {
    if (!req.query.token)
        return error(req, res, "Missing argument", 400);
    const user = await models.User.findOne({where: {emailToken: req.query.token}});
    if (user) {
        user.emailVerified = true;
        if (user.email.endsWith("@etu.univ-lyon1.fr"))
            user.permissions = 1;
        else if (user.email.endsWith("@univ-lyon1.fr"))
            user.permissions = 2;
        await user.save();
        res.redirect("/");
    } else
        return error(req, res, "Invalid token", 400);
});

router.get("/forget", sessionCheck(-1), async (req, res) => {
    if (!req.query.token)
        res.render("forget", {title: "L'ETU"});
    else {
        const user = await models.User.findOne({where: {passwordToken: data.token}});
        if (!user)
            return error(req, res, "Invalid token", 400);
        else if (user.passwordTokenDate && ((new Date().getTime() - user.passwordTokenDate.getTime()) / 1000 > 3600))
            return error(req, res, "Token expired", 400);
        else
            res.render("forget", {title: "L'ETU - Forget password"});
    }
});

module.exports = router;
