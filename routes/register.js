const express = require("express");
const router = express.Router();
const sessionCheck = require("./utils/sessionCheck");

router.get("/",sessionCheck(-1), (req, res) => {
    res.render("pages/register", { title: "L'ETU" });
});

module.exports = router;