const express = require("express");
const sessionCheck = require("./utils/sessionCheck");
const router = express.Router();

router.get("/", sessionCheck(1), (req, res) => {
    res.render("pages/profil", { title: "L'ETU" });
});

module.exports = router;