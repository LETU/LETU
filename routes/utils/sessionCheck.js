const error = require("./error");

function sessionCheck(permission) {
    return (req, res, next) => {
        if (permission === -1 && req.session.user) {
            res.redirect("/");
        } else if (permission !== -1 && !req.session.user) {
            req.session.lastUrl = req.originalUrl;
            req.session.save(() => res.redirect("/login"));
        } else if (req.session.user && req.session.user.permissions < permission) {
            return error(req, res, "Permission denied !", 403);
        } else
            next();
    }
}

module.exports = sessionCheck;
