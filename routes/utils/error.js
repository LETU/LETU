module.exports = (req, res, message, status, subMessage) => {
    res.status(status || 500);
    res.render("template/error", {message: message, error: {status: subMessage || undefined}});
};
