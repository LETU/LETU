const { Worker } = require("worker_threads");
let nb = 0;


function startWorker(workerArgs) {
    let worker = new Worker(...workerArgs);
    worker.on("error", (err) => {
        console.error(err);
        nb--;
        if (nb > 0)
            startWorker(workerArgs);
    });
}

module.exports = (app) => {
    const workerArgs = ["./agenda/worker.js", {workerData: app.get("config")}];
    startWorker(workerArgs);
};
