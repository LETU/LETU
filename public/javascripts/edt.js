const date = document.getElementById("date");
const details = document.getElementById("details");
const marksdetailsbackground = document.getElementById("marksdetailsbackground");
const events = {};
let p = 0;

socket.on("agendaGet", data => {
    document.querySelectorAll(".eventName, .eventLocation").forEach(p => {
        // Clear
        p.innerHTML = "";
        // Mr Propre
    });
    document.querySelectorAll("td").forEach(td => td.dataset.id = undefined);
    /* Populate that shit */
    data.forEach(event => {
        let day = new Date(event.startDate);
        let x = day.getDay()-1;
        let y = day.getHours();
        if (y >= 8 && y < 10)
            y = 0;
        else if (y >= 10 && y < 12)
            y = 1;
        else if (y >= 12 && y < 14)
            y = 2;
        else if (y >= 14 && y < 16)
            y = 3;
        else if (y >= 16 && y < 18)
            y = 4;
        else
            y = null;
        if (y != null) {
            let el = document.getElementById(`${y}${x}`);
            el.dataset.id = event.id;
            el.querySelector(".eventName").innerHTML = event.name;
            el.querySelector(".eventLocation").innerHTML = event.locations;
        }
        events[event.id] = event;
    });
});

function update() {
    let startDate = new Date();
    let endDate = new Date();
    startDate.setHours(0, 0, 0, 0);
    endDate.setHours(0, 0, 0, 0);
    startDate.setDate((startDate.getDate()-startDate.getDay()+1)+(p*7));
    endDate.setDate((endDate.getDate()+(7-endDate.getDay()))+(p*7));
    socket.emit("agendaGet", { groups: groups.map(g => g.id), startDate: startDate, endDate: endDate});
    date.innerHTML = "Week of the " + startDate.toISOString().slice(0, 10);
}

function detail(id) {
    let e = events[document.getElementById(id).dataset.id];
    if (e) {
        details.querySelector(".eventName").innerHTML = e.name;
        details.querySelector(".eventLocation").innerHTML = e.locations;
        details.querySelector(".eventTeacher").innerHTML = e.Users.map(u => u.firstName + " " + u.lastName).join(" - ");
        details.querySelector(".eventTarget").innerHTML = e.Groups.map(g => g.displayName).concat(e.Semesters.map(s => s.name)).join(" - ");
        details.querySelector(".eventTime").innerHTML = new Date(e.startDate).toTimeString().slice(0, 5) + " " + new Date(e.endDate).toTimeString().slice(0, 5);
        details.querySelector(".eventUE").innerHTML = e.UE ? e.UE.name : "";
        details.classList.remove("invisible");
        marksdetailsbackground.classList.remove("invisible");
    }
}

document.addEventListener("DOMContentLoaded", () => {
    update()
});

document.getElementById("previous").addEventListener("click", () => {
    p--;
    update();
});

document.getElementById("next").addEventListener("click", () => {
    p++;
    update();
});

document.querySelectorAll("td").forEach(td => {
    td.addEventListener("click", () => detail(td.id))
});

marksdetailsbackground.addEventListener("click", () => {
    details.classList.add("invisible");
    marksdetailsbackground.classList.add("invisible");
});
