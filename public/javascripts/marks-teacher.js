const group = document.getElementById("group");
const evaluation = document.getElementById("evaluation");
const marksform = document.getElementById("marksform");
const markstable = document.getElementById("markstable").querySelector("tbody");
const studentTemplate = document.getElementById("studentTemplate");

let evaluations = {};
let students = [];

group.addEventListener("change", () => socket.emit("evaluationGet"));

socket.on("evaluationGet", evals => {
	evaluations = {};
	evaluation.innerHTML = "<option selected disabled></option>";
	for (let ev of evals) {
		if (!document.getElementById("UE"+ev.UE.id)) {
			evaluation.insertAdjacentHTML("beforeend", `<optgroup id="UE${ev.UE.id}" label="${ev.UE.name}"></optgroup>`)
		}
		document.getElementById("UE"+ev.UE.id).insertAdjacentHTML("beforeend", `<option value="${ev.id}">${ev.name}</option>`);
		evaluations[ev.id] = ev;
	}
	M.FormSelect.init(evaluation);
});

evaluation.addEventListener("change", () => {
	marksform.querySelector(".evaluationName").innerHTML = evaluations[evaluation.value].UE.name + " - " + evaluations[evaluation.value].name;
	socket.emit("studentGet", group.value);
});

socket.on("studentGet", sts => {
	markstable.querySelectorAll("tr:not(.studentTemplate)").forEach(el => el.remove());
	for (let st of sts) {
		let el = studentTemplate.cloneNode(true);
		el.style = "";
		el.id = "Student"+st.email;
		el.querySelector(".studentName").innerHTML = st.firstName + " " + st.lastName;

		let gs = el.querySelector(".gradeScore");
		gs.name = "score"+st.email;
		let g = evaluations[evaluation.value].Grades.find(s => s.StudentGradeEmail === st.email);
		if (g)
			gs.value = g.score;

		students.push(st);
		markstable.insertAdjacentElement("beforeend", el);
	}
});

marksform.addEventListener("submit", e => {
	e.preventDefault();
	let grds = {};
	for (let e of new FormData(marksform))
		grds[e[0].replace(/^score/i, "")] = e[1];
	socket.emit("gradeSet", {evaluation: evaluations[evaluation.value].id, grades: grds});
});
