socket.on("agendaGet", data => {
    document.querySelectorAll(".eventName, .eventLocation").forEach(p => p.innerHTML = "");
    data.forEach(event => {
        let day = new Date(event.startDate);
        let y = day.getHours();
        if (y >= 8 && y < 10)
            y = 0;
        else if (y >= 10 && y < 12)
            y = 1;
        else if (y >= 12 && y < 14)
            y = 2;
        else if (y >= 14 && y < 16)
            y = 3;
        else if (y >= 16 && y < 18)
            y = 4;
        else
            y = null;
        if (y != null) {
            let el = document.getElementById(`${y}`);
            el.dataset.id = event.id;
            el.querySelector(".eventName").innerHTML = event.name;
            el.querySelector(".eventLocation").innerHTML = event.locations;
        }
    });
});

document.addEventListener("DOMContentLoaded", () => {
    let startDate = new Date();
    let endDate = new Date();
    startDate.setHours(0, 0, 0, 0);
    endDate.setHours(23, 0, 0, 0);
    socket.emit("agendaGet", { groups: groups.map(g => g.id), startDate: startDate, endDate: endDate});
});
