const marks = document.getElementById("marksrow");
const evaluationTemplate = marks.querySelector("#evaluationTemplate");
const gradeTemplate = marks.querySelector("#gradeTemplate");
const details = document.getElementById("marksdetails");
const detailsBackground = document.getElementById("marksdetailsbackground");

let grades = [];

document.addEventListener("DOMContentLoaded", () => socket.emit("gradeGet"));
detailsBackground.addEventListener("click", invisible);

socket.on("gradeGet", data=>{
	for (let grade of data)
		grades.push(grade);
	updateGrades()
});


function updateGrades() {
	for (let grade of grades) {
		let e = document.getElementById("UE"+grade.Evaluation.UE.id);
		if (!e) {
			let temp = evaluationTemplate.cloneNode(true);
			temp.querySelector(".evaluationName").innerHTML = grade.Evaluation.UE.name;
			temp.id = "UE"+grade.Evaluation.UE.id;
			temp.classList.remove("invisible");
			temp = marks.insertAdjacentElement("beforeend", temp);
			e = temp;
		}
		let g = gradeTemplate.cloneNode(true);
		g.querySelector(".gradeName").innerHTML = grade.Evaluation.name;
		g.querySelector(".gradeMark").innerHTML = grade.score + "/" + grade.limit;
		g.querySelector(".gradeComment").innerHTML = grade.comment ? grade.comment : "";
		g.id = "Grade"+grade.Evaluation.id;
		g.classList.remove("invisible");
		let score = grade.score;
		if (grade.limit != 20)
			score = (20/grade.limit)*grade.score;
		if (score < 4)
			g.classList.add("marks-4");
		else if (score < 6)
			g.classList.add("marks-6");
		else if (score < 8)
			g.classList.add("marks-8");
		else if (score < 10)
			g.classList.add("marks-10");
		else if (score < 12)
			g.classList.add("marks-12");
		else if (score < 14)
			g.classList.add("marks-14");
		else if (score < 16)
			g.classList.add("marks-16");
		else if (score < 18)
			g.classList.add("marks-18");
		else if (score < 20)
			g.classList.add("marks-20");
		g.addEventListener("click", () => gradeResume(grade));
		e.querySelector(".grades").insertAdjacentElement("beforeend", g);
	}
}

function gradeResume(grade) {
	details.querySelector("#dsName").innerHTML = grade.Evaluation.name;
	details.querySelector("#dsMarks").innerHTML = grade.score + "/" + grade.limit;
	details.querySelector("#dsComment").innerHTML = grade.comment ? grade.comment : "";
	visible();
}

function visible() {
	details.classList.remove("invisible");
	detailsBackground.classList.remove("invisible");
}

function invisible() {
	details.classList.add("invisible");
	detailsBackground.classList.add("invisible");
}
