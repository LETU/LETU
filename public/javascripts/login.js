document.getElementById("login").addEventListener("submit", e=>{
	e.preventDefault();
	let expressionReguliere = /^(([^<>()[]\.,;:s@]+(.[^<>()[]\.,;:s@]+)*)|(.+))@(([[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}])|(([a-zA-Z-0-9]+.)+[a-zA-Z]{2,}))$/;
	if(expressionReguliere.test(document.getElementById("mail-input").value)){
		let email = document.getElementById("mail-input").value;
		let passw = document.getElementById("password-input").value;
		socket.emit("login", {"email":email,"password":passw});
	}else{
		alert('Format d\'email incorrect.');
	}
	return false;
});

socket.on("login", data=>{
	if(data.error){
		alert(data.error.message);
	}else{
		window.location.href = "/";
	}
});