document.getElementById("editprofil").addEventListener("submit", e=>{
	e.preventDefault();
	socket.emit("profileEdit", {
        "email": mail,
        "firstName": document.getElementById("firstname-input").value,
        "lastName": document.getElementById("lastname-input").value,
		"newPassword": document.getElementById("new-password").value,
        "password": document.getElementById("password").value
	});
});


socket.on("profileEdit", data=>{
	if(data.error){
		alert(data.error.message);
	}else{
		window.location.href = "/";
	}
});