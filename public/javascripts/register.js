document.getElementById("register").addEventListener("submit", e=>{
	e.preventDefault();
	let expressionReguliere = /^(([^<>()[]\.,;:s@]+(.[^<>()[]\.,;:s@]+)*)|(.+))@(([[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}])|(([a-zA-Z-0-9]+.)+[a-zA-Z]{2,}))$/;
	let mail = document.getElementById("mailreg-input").value;
	if(expressionReguliere.test(mail)) {
		socket.emit("register", {
            "email": mail,
            "firstName": document.getElementById("firstname-input").value,
            "lastName": document.getElementById("lastname-input").value,
            "password": document.getElementById("passwordreg-input").value
		});
	}
});

socket.on("register", data=>{
	if(data.error){
		alert(data.error.message);
	}else{
		window.location.href = "/";
	}
});
