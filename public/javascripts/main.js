const socket = io.connect();
/*
    Front-end Event
 */
// Disconnect
document.addEventListener("DOMContentLoaded", () => {
    const logout = document.getElementById("logout");
    if (logout)
        logout.addEventListener("click", () => socket.emit("logout"));
});


/*
    Socket Actions
 */
// Disconnect
socket.on("logout", data=>{
    if(data.error){
        alert(data.error.message);
    }else{
        window.location.href = "/";
    }
});


function profilRedirect(){
    document.location.href="/profil";
}
